package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {

        String str = "";

        for (String s : args) {
            str += s + " ";
        }
        return str;
    }

    @Override
    public String getName() {
        String str = "echo";
        return str;
    }
    
}
